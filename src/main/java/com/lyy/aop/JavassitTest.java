package com.lyy.aop;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

class Test1 {

    public final void test() {
        System.out.println("test------------->");
    }

}

public class JavassitTest {

    public static void main(String[] args) throws Exception {
        ClassPool cp = ClassPool.getDefault();
        CtClass cc = cp.get("com.lyy.aop.Test1");
        CtMethod m = cc.getDeclaredMethod("test");
        m.insertAfter("System.out.println(\"before------------->\");");
        m.insertBefore("System.out.println(\"after------------->\");");
        Class c = cc.toClass();
        Test1 test1 = (Test1)c.newInstance();
        test1.test();
    }

}