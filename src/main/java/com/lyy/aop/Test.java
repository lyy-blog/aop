package com.lyy.aop;

import com.lyy.Advice;

/**
 * 创建测试类
 */
public class Test {

    @Advice
    public final void test() {
        System.out.println("test------------->");
    }

    public static void main(String[] args){
        Test test=new Test();
        test.test();
    }

}