package com.lyy.dynamicproxy;

/**
 * description:
 *
 * @author ${user}
 * @date 2017/12/4
 */
public class Man implements IHuman {

    @Override
    public void hello() {
        System.out.println("hello i am a man");
    }
}
