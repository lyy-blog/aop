package com.lyy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * description:
 *
 * @author ${user}
 * @date 2017/12/4
 */
public class MyInvocationHandler implements InvocationHandler {

    private Object object;

    public MyInvocationHandler(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("before method:"+method.getName());
        Object res = method.invoke(object, args);
        System.out.println("after method:"+method.getName());
        return res;
    }

}
