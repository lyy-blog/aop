package com.lyy.dynamicproxy;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.Proxy;

/**
 * description:
 *
 * @author ${user}
 * @date 2017/12/4
 */
public class DynamicProxyMain {

    public static void main(String[] args) {

        IHuman man=new Man();
        MyInvocationHandler handler = new MyInvocationHandler(man);

        ClassLoader loader=man.getClass().getClassLoader();
        Class[] interfaces=man.getClass().getInterfaces();

        IHuman human= (IHuman) Proxy.newProxyInstance(loader, interfaces, handler);
        human.hello();


       createProxyClassFile();
    }

    // 获取代理类字节码
    public static void createProxyClassFile()
    {
        String name = "ProxySubject";
        byte[] data = ProxyGenerator.generateProxyClass( name, new Class[] { IHuman.class } );
        try
        {
            FileOutputStream out = new FileOutputStream( name + ".class" );
            out.write( data );
            out.close();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

}
